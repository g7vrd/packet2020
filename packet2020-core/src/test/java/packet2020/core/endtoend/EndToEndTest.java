package packet2020.core.endtoend;

import org.junit.Test;
import packet2020.core.byteencoders.ByteEncoder;
import packet2020.core.byteencoders.olivia.ByteToOliviaEncoder;
import packet2020.core.compressors.Compressor;
import packet2020.core.compressors.snappy.SnappyCompressor;
import packet2020.core.fec.ErrorCorrector;
import packet2020.core.fec.hamming.Hamming74ErrorCorrector;
import packet2020.core.signers.Md5Signer;
import packet2020.core.signers.Signer;

import static org.assertj.core.api.Assertions.assertThat;

public class EndToEndTest {

  private final Compressor compressor = new SnappyCompressor();
  private final Signer md5Signer = new Md5Signer(2);
  private final ErrorCorrector errorCorrector = new Hamming74ErrorCorrector();
  private final ByteEncoder encoder = new ByteToOliviaEncoder();

  @Test
  public void EndToEnd() {
    final byte[] input = "The input data".getBytes();

    final byte[] compressed = compressor.compress(input);
    final byte[] signed = md5Signer.addSignature(compressed);
    final byte[] errorCorrected = errorCorrector.addFEC(signed);
    final char[] encoded = encoder.encode(errorCorrected);

    // Corrupt - was 'a'
    encoded[4] = 'e';


    final byte[] decoded = encoder.decode(encoded);
    final byte[] corrected = errorCorrector.getCorrected(decoded);
    final byte[] verified = md5Signer.verify(corrected);
    final byte[] decompressed = compressor.decompress(verified);


    assertThat(decompressed).containsExactly(input);
  }
}
