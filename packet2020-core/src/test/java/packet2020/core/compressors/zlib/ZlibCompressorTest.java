package packet2020.core.compressors.zlib;

import org.junit.Ignore;
import org.junit.Test;
import packet2020.core.compressors.Compressor;

import java.util.Random;

import static org.assertj.core.api.Assertions.assertThat;

public class ZlibCompressorTest {

//  private final Compressor compressor = new ZlibCompressor();
  private final Compressor compressor = null;

  @Ignore
  @Test
  public void Data_is_compressed_and_decompressed_correctly_simple() {
    byte[] input = "AAAAAAAAAAAAAAAAAAAAAAAAA".getBytes();

    final byte[] compressed = compressor.compress(input);
    final byte[] decompressed = compressor.decompress(compressed);

    assertThat(decompressed).hasSameSizeAs(input);
    assertThat(decompressed).containsExactly(input);
  }

  @Ignore
  @Test
  public void Data_is_compressed_and_decompressed_correctly() {
    byte[] input = getRandomMessage(8192);
    final byte[] compressed = compressor.compress(input);

    final byte[] decompressed = compressor.decompress(compressed);

    assertThat(decompressed).hasSameSizeAs(input);
    assertThat(decompressed).containsExactly(input);
  }


  private byte[] getRandomMessage(int length) {
    byte[] message = new byte[length];
    new Random(System.currentTimeMillis()).nextBytes(message);
    return message;
  }
}
