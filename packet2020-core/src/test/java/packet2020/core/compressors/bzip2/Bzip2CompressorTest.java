package packet2020.core.compressors.bzip2;

import org.junit.Test;
import packet2020.core.compressors.Compressor;

import java.util.Random;

import static org.assertj.core.api.Assertions.assertThat;

public class Bzip2CompressorTest {

  private final Compressor compressor = new Bzip2Compressor();

  @Test
  public void Data_is_compressed_and_decompressed_correctly() {
    byte[] input = getRandomMessage(8192);

    final byte[] compressed = compressor.compress(input);
    final byte[] decompressed = compressor.decompress(compressed);

    assertThat(decompressed).hasSameSizeAs(input);
    assertThat(decompressed).containsExactly(input);
  }


  private byte[] getRandomMessage(int length) {
    byte[] message = new byte[length];
    new Random(System.currentTimeMillis()).nextBytes(message);
    return message;
  }
}
