package packet2020.core.compressors;

import org.junit.Test;
import packet2020.core.compressors.bzip2.Bzip2Compressor;
import packet2020.core.compressors.gzip.GzipCompressor;
import packet2020.core.compressors.snappy.SnappyCompressor;

public class CompressionComparisonTest {

  // Typically, the amount of data will be quite short

  @Test
  public void Comparison_of_compressors_with_1_byte() {
    byte[] input = "x".getBytes();

//    System.out.println("ZLIB: " + new ZlibCompressor().compress(input).length);
    System.out.println("Tiny:   Snappy: " + new SnappyCompressor().compress(input).length);
    System.out.println("Tiny:   GZIP : " + new GzipCompressor().compress(input).length);
    System.out.println("Tiny:   BZIP2: " + new Bzip2Compressor().compress(input).length);
  }

  @Test
  public void Comparison_of_compressors_with_short_data() {
    byte[] input = "CALLSIGN|Some text xxxxxxxxxxx".getBytes();

//    System.out.println("ZLIB: " + new ZlibCompressor().compress(input).length);
    System.out.println("Short:  Snappy: " + new SnappyCompressor().compress(input).length);
    System.out.println("Short:  GZIP : " + new GzipCompressor().compress(input).length);
    System.out.println("Short:  BZIP2: " + new Bzip2Compressor().compress(input).length);
  }

  @Test
  public void Comparison_of_compressors_with_medium_length_data() {
    byte[] input = "CALLSIGN|1591092923|Hello, this is my message. It's mainly ASCII:SIG:d3b07384d113edec49eaa6238ad5ff00".getBytes();

//    System.out.println("ZLIB: " + new ZlibCompressor().compress(input).length);
    System.out.println("Medium: Snappy: " + new SnappyCompressor().compress(input).length);
    System.out.println("Medium: GZIP : " + new GzipCompressor().compress(input).length);
    System.out.println("Medium: BZIP2: " + new Bzip2Compressor().compress(input).length);
  }

  @Test
  public void Comparison_of_compressors_with_long_data() {
    byte[] input = ("Huffman coding is not the only encoding scheme to use variable-length code based on the probability" +
      " of occurrence of the characters in a message.  For example, the International Morse Code, originally created by " +
      "Samuel Morse in the mid-1830s is a variable-length code, apparently based on his concept of the probability of " +
      "occurrence of the letters in the English alphabet.  For example, the code for the letter E is the shortest code.  " +
      "The letter E occurs very frequently in English text.  The longest code is for the number 0.  Excluding the " +
      "numbers, however, the two longest codes are for the letters Q and W.  The letter Q doesn't appear very often in " +
      "English Text.").getBytes();

//    System.out.println("ZLIB: " + new ZlibCompressor().compress(input).length);
    System.out.println("Long:   GZIP  : " + new GzipCompressor().compress(input).length);
    System.out.println("Long:   BZIP2 : " + new Bzip2Compressor().compress(input).length);
    System.out.println("Long:   Snappy: " + new SnappyCompressor().compress(input).length);
  }

}
