package packet2020.core.signers;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import packet2020.core.SigningException;

import static org.assertj.core.api.Assertions.assertThat;

public class Md5SignerTest {

  @Rule
  public ExpectedException expectedException = ExpectedException.none();

  @Test
  public void Md5Signer_can_create_and_verify_good_signatures() {
    final Signer signer = new Md5Signer(10);
    final byte[] original = "test".getBytes();

    final byte[] dataWithSignature = signer.addSignature(original);

    final byte[] verifiedData = signer.verify(dataWithSignature);

    assertThat(verifiedData).containsExactly(original);
  }

  @Test
  public void Md5Signer_throws_exception_if_signature_is_bad() {
    final Signer signer = new Md5Signer(10);
    final byte[] original = "test".getBytes();

    final byte[] dataWithSignature = signer.addSignature(original);

    // Corrupt the signature
    dataWithSignature[7] = (byte) 141;

    expectedException.expect(SigningException.class);

    signer.verify(dataWithSignature);
  }

  @Test
  public void Md5Signer_throws_exception_if_data_is_bad() {
    final Signer signer = new Md5Signer(10);
    final byte[] original = "test".getBytes();

    final byte[] dataWithSignature = signer.addSignature(original);

    // Corrupt the data
    dataWithSignature[2] = (byte) 141;

    expectedException.expect(SigningException.class);

    signer.verify(dataWithSignature);
  }

  @Test
  public void Md5Signer_throws_exception_if_data_is_too_short_to_contain_signature() {
    final Signer signer = new Md5Signer(10);
    final byte[] input = "test".getBytes();

    expectedException.expect(SigningException.class);
    expectedException.expectMessage("Input is not long enough (4) to contain a signature (10) and data");

    signer.verify(input);
  }

}
