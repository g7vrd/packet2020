package packet2020.core.byteencoders.olivia;

import org.junit.Test;
import packet2020.core.byteencoders.ByteEncoder;

import java.util.Random;

import static org.assertj.core.api.Assertions.assertThat;

public class ByteToOliviaEncoderTest {

  private final ByteEncoder oliviaEncoder = new ByteToOliviaEncoder();

  private final byte[] message = getRandomMessage();

  @Test
  public void Olivia_encoder_works() {
    final char[] encoded = oliviaEncoder.encode(message);

    final byte[] decoded = oliviaEncoder.decode(encoded);

    assertThat(decoded).containsExactly(message);

    System.out.println("Olivia    " + getSymbols(encoded) + " " + (getSymbols(encoded) / (float) message.length) + " symbols per byte");
  }


  private int getSymbols(char[] chars) {
    int symbols = 0;
    for (char c : chars) {
      symbols = symbols + OliviaVaricodeLengths.getLength(c);
    }

    return symbols;
  }


  private byte[] getRandomMessage() {
    byte[] message = new byte[255];
    new Random(System.currentTimeMillis()).nextBytes(message);
    return message;
  }

}
