package packet2020.core.byteencoders.iso88591;

import org.junit.Test;
import packet2020.core.byteencoders.ByteEncoder;
import packet2020.core.byteencoders.olivia.OliviaVaricodeLengths;

import java.util.Random;

import static org.assertj.core.api.Assertions.assertThat;

public class ByteToISO88591EncoderTest {

  private final ByteEncoder iso88591Encoder = new ByteToISO88591Encoder();

  private final byte[] message = getRandomMessage();


  @Test
  public void ISO8859_1_encoder_works() {
    final char[] encoded = iso88591Encoder.encode(message);

    final byte[] decoded = iso88591Encoder.decode(encoded);

    assertThat(decoded).containsExactly(message);

    System.out.println("ISO8895-1 " + getSymbols(encoded) + " " + (getSymbols(encoded) / (float) message.length) + " symbols per byte");
  }

  private int getSymbols(char[] chars) {
    int symbols = 0;
    for (char c : chars) {
      symbols = symbols + OliviaVaricodeLengths.getLength(c);
    }

    return symbols;
  }


  private byte[] getRandomMessage() {
    byte[] message = new byte[255];
    new Random(System.currentTimeMillis()).nextBytes(message);
    return message;
  }

}
