package packet2020.core.fec.triple;

import org.junit.Test;
import packet2020.core.fec.ErrorCorrector;

import java.util.Arrays;
import java.util.BitSet;

import static org.assertj.core.api.Assertions.assertThat;

public class TripleCopyErrorCorrectorTest {

  private final ErrorCorrector errorCorrector = new TripleCopyErrorCorrector();

  @Test
  public void TripleErrorCorrector_works_as_expected() {
    final byte[] input = "SOME TEST data".getBytes();

    final byte[] errorCorrected = errorCorrector.addFEC(input);

    // Corrupt a few bits
    final BitSet bitSet = BitSet.valueOf(errorCorrected);
    bitSet.flip(141);
    bitSet.flip(92);
    bitSet.flip(12);
    bitSet.flip(222);
    final byte[] corrupted = bitSet.toByteArray();

    assertThat(Arrays.equals(corrupted, errorCorrected)).isFalse();

    final byte[] corrected = errorCorrector.getCorrected(corrupted);

    assertThat(corrected).containsExactly(input);
  }

}
