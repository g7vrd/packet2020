package packet2020.core.fec.hamming;

import org.junit.Test;
import packet2020.core.fec.ErrorCorrector;

import java.util.Arrays;
import java.util.BitSet;
import java.util.Random;

import static org.assertj.core.api.Assertions.assertThat;

public class Hamming74ErrorCorrectorTest {

  private final ErrorCorrector errorCorrector = new Hamming74ErrorCorrector();

  private final Random random = new Random(System.currentTimeMillis());

  @Test
  public void Hamming74ErrorCorrector_works_as_expected() {
    final byte[] input = new byte[random.nextInt(4096)];
    random.nextBytes(input);

    final byte[] errorCorrected = errorCorrector.addFEC(input);

    // Corrupt a few bits
    final BitSet bitSet = BitSet.valueOf(errorCorrected);
    bitSet.flip(141);
    bitSet.flip(92);
    bitSet.flip(12);
    bitSet.flip(222);
    final byte[] corrupted = bitSet.toByteArray();

    assertThat(Arrays.equals(corrupted, errorCorrected)).isFalse();

    final byte[] corrected = errorCorrector.getCorrected(corrupted);

    assertThat(corrected).containsExactly(input);
  }
}
