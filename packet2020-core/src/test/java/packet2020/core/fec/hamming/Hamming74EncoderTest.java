package packet2020.core.fec.hamming;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class Hamming74EncoderTest {

  private final Hamming74Encoder hamming74Encoder = new Hamming74Encoder();

  @Test
  public void Eleven_is_correctly_encoded() {
    boolean[] result = hamming74Encoder.encode(true, false, true, true);


    assertThat(result).containsExactly(false, true, true, false, false, true, true);
  }
}
