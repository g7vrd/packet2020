package packet2020.core.fec.hamming;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class Hamming74DecoderTest {

  private final Hamming74Decoder hamming74Decoder = new Hamming74Decoder();

  @Test
  public void Wikipedia_example() {
    final boolean[] decoded = hamming74Decoder.decode(false, true, true, false, false, true, true);

    assertThat(decoded).containsExactly(true, false, true, true, false, false, false);
  }

  @Test
  public void Wikipedia_example_bit_1_error() {
    final boolean[] decoded = hamming74Decoder.decode(false, true, true, false, false, true, false);

    assertThat(decoded).containsExactly(true, false, true, true, false, false, false);
  }

  @Test
  public void Wikipedia_example_bit_2_error() {
    final boolean[] decoded = hamming74Decoder.decode(false, true, true, false, false, false, true);

    assertThat(decoded).containsExactly(true, false, true, true, false, false, false);
  }


  @Test
  public void Wikipedia_example_bit_3_error() {
    final boolean[] decoded = hamming74Decoder.decode(false, true, true, false, true, true, true);

    assertThat(decoded).containsExactly(true, false, true, true, false, false, false);
  }


  @Test
  public void Wikipedia_example_bit_4_error() {
    final boolean[] decoded = hamming74Decoder.decode(false, true, true, false, false, true, true);

    assertThat(decoded).containsExactly(true, false, true, true, false, false, false);
  }


  @Test
  public void Wikipedia_example_bit_5_error() {
    final boolean[] decoded = hamming74Decoder.decode(false, true, false, false, false, true, true);

    assertThat(decoded).containsExactly(true, false, true, true, false, false, false);
  }


  @Test
  public void Wikipedia_example_bit_6_error() {
    final boolean[] decoded = hamming74Decoder.decode(false, false, true, false, false, true, true);

    assertThat(decoded).containsExactly(true, false, true, true, false, false, false);
  }


  @Test
  public void Wikipedia_example_bit_7_error() {
    final boolean[] decoded = hamming74Decoder.decode(true, true, true, false, false, true, true);

    assertThat(decoded).containsExactly(true, false, true, true, false, false, false);
  }


}
