package packet2020;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import packet2020.core.receiver.Receiver;
import packet2020.core.sender.Sender;


@SpringBootApplication
public class Packet2020Application implements CommandLineRunner {

  private final Logger logger = LoggerFactory.getLogger(getClass());

  private final Sender sender;
  private final Receiver receiver;
  private final String callsign;

  public static void main(String[] args) {
    final SpringApplication springApplication = new SpringApplicationBuilder()
      .main(Packet2020Application.class)
      .sources(Packet2020Application.class)
      .web(WebApplicationType.NONE)
      .build(args);

    springApplication.run(args);
  }

  @Autowired
  public Packet2020Application(Sender sender,
                               Receiver receiver,
                               @Value("${station.callsign}") String callsign) {
    this.sender = sender;
    this.receiver = receiver;
    this.callsign = callsign;
    logger.info("Initialised with station.callsign " + callsign);
  }

  @Override
  public void run(String... args) {
    receiver.start();
//    sender.send((callsign + "|" + getTimestamp() + "|Hi").getBytes());
  }


  private String getTimestamp() {
    final long millis = System.currentTimeMillis();

    return "" + millis;
  }

  private void zzzz(int millis) {
    try {
      Thread.sleep(millis);
    } catch (InterruptedException e) {
      throw new RuntimeException(e);
    }
  }

}
