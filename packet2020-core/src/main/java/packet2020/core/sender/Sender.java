package packet2020.core.sender;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import packet2020.client.api.Client;
import packet2020.client.api.Mode;
import packet2020.client.api.OliviaModem;
import packet2020.core.byteencoders.ByteEncoder;
import packet2020.core.compressors.Compressor;
import packet2020.core.fec.ErrorCorrector;
import packet2020.core.signers.Signer;

@Service
public class Sender {


  private final Client client;
  private final Compressor compressor;
  private final ByteEncoder byteEncoder;
  private final ErrorCorrector errorCorrector;
  private final Signer signer;

  @Autowired
  public Sender(Client client,
                Compressor compressor,
                ByteEncoder byteEncoder,
                ErrorCorrector errorCorrector,
                Signer signer) {
    this.client = client;
    this.compressor = compressor;
    this.byteEncoder = byteEncoder;
    this.errorCorrector = errorCorrector;
    this.signer = signer;
  }


  public void send(byte[] bytes) {
    final byte[] compressed = compressor.compress(bytes);
    // encrypt? Not for amateur radio obviously
    final byte[] signed = signer.addSignature(compressed);
    final byte[] errorCorrected = errorCorrector.addFEC(signed);
    final char[] encoded = byteEncoder.encode(errorCorrected);

    System.out.println("OUT: >>>" + new String(encoded) + "<<<");

    client.setFrequency(14105540);
    client.setMode(Mode.PKTUSB);
    client.setModem(OliviaModem.OLIVIA_32_1K);
    client.setModemCarrierFrequency(1000);
    client.clearRxText();
    client.clearTxText();
    client.addTxText(new String(encoded) + "^r");
    client.setRsId(true);
    client.setTxId(true);

    client.transmit();
  }


}
