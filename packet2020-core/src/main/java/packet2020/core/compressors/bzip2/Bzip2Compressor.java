package packet2020.core.compressors.bzip2;

import org.apache.commons.compress.compressors.bzip2.BZip2CompressorInputStream;
import org.apache.commons.compress.compressors.bzip2.BZip2CompressorOutputStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import packet2020.core.compressors.Compressor;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

public class Bzip2Compressor implements Compressor {

  private final Logger logger = LoggerFactory.getLogger(getClass());

  @Override
  public byte[] compress(byte[] input) {
    try (final ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
         final BZip2CompressorOutputStream compressorOutputStream = new BZip2CompressorOutputStream(outputStream, BZip2CompressorOutputStream.MAX_BLOCKSIZE)) {

      compressorOutputStream.write(input);
      compressorOutputStream.close();

      final byte[] bytes = outputStream.toByteArray();
      logger.info("Compressed " + input.length + " bytes to " + bytes.length + " bytes");
      return bytes;
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  @Override
  public byte[] decompress(byte[] input) {
    try (ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
         BZip2CompressorInputStream zipStream = new BZip2CompressorInputStream(new ByteArrayInputStream(input))) {

      byte[] buffer = new byte[1024];
      int len;
      while ((len = zipStream.read(buffer)) != -1) {
        outputStream.write(buffer, 0, len);
      }

      final byte[] bytes = outputStream.toByteArray();
      logger.info("Decompressed " + input.length + " bytes to " + bytes.length + " bytes");
      return bytes;
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }
}
