package packet2020.core.compressors.zlib;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import packet2020.core.compressors.Compressor;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.zip.DeflaterInputStream;
import java.util.zip.DeflaterOutputStream;

public class ZlibCompressor implements Compressor {

  private final Logger logger = LoggerFactory.getLogger(getClass());

  private ZlibCompressor() {
    // Doesn't work correctly at the moment - see test
  }

  @Override
  public byte[] compress(byte[] input) {
    try (ByteArrayOutputStream output = new ByteArrayOutputStream(input.length);
         DeflaterOutputStream zipStream = new DeflaterOutputStream(output)) {
      zipStream.write(input);
      zipStream.finish();

      final byte[] bytes = output.toByteArray();
      logger.info("Compressed " + input.length + " bytes to " + bytes.length + " bytes");
      return bytes;
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  @Override
  public byte[] decompress(byte[] input) {
    try (ByteArrayOutputStream output = new ByteArrayOutputStream();
         DeflaterInputStream zipStream = new DeflaterInputStream(new ByteArrayInputStream(input))) {

      byte[] buffer = new byte[1024];
      int len;
      while ((len = zipStream.read(buffer)) != -1) {
        output.write(buffer, 0, len);
      }

      final byte[] bytes = output.toByteArray();
      logger.info("Decompressed " + input.length + " bytes to " + bytes.length + " bytes");
      return bytes;
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }
}
