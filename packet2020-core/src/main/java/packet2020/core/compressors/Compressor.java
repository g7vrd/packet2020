package packet2020.core.compressors;

public interface Compressor {

  byte[] compress(byte[] input);

  byte[] decompress(byte[] input);
}
