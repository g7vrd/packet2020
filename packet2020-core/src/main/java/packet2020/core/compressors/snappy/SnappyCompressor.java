package packet2020.core.compressors.snappy;

import org.apache.commons.compress.compressors.snappy.SnappyCompressorInputStream;
import org.apache.commons.compress.compressors.snappy.SnappyCompressorOutputStream;
import org.springframework.stereotype.Component;
import packet2020.core.compressors.Compressor;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

@Component
public class SnappyCompressor implements Compressor {

  @Override
  public byte[] compress(byte[] input) {
    try (ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
         SnappyCompressorOutputStream zipStream = new SnappyCompressorOutputStream(byteStream, input.length)) {
      zipStream.write(input);
      zipStream.finish();
      zipStream.close();

      return byteStream.toByteArray();
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  @Override
  public byte[] decompress(byte[] input) {
    try (ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
         SnappyCompressorInputStream zipStream = new SnappyCompressorInputStream(new ByteArrayInputStream(input))) {

      byte[] buffer = new byte[14];
      int len;
      while ((len = zipStream.read(buffer)) != -1) {
        outputStream.write(buffer, 0, len);
      }

      return outputStream.toByteArray();
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }
}
