package packet2020.core.compressors.gzip;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import packet2020.core.compressors.Compressor;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

public class GzipCompressor implements Compressor {

  private final Logger logger = LoggerFactory.getLogger(getClass());

  @Override
  public byte[] compress(byte[] input) {
    try (ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
         GZIPOutputStream zipStream = new GZIPOutputStream(byteStream)) {
      zipStream.write(input);
      zipStream.finish();
      zipStream.close();

      final byte[] bytes = byteStream.toByteArray();
      logger.info("Compressed " + input.length + " bytes to " + bytes.length + " bytes");
      return bytes;
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  @Override
  public byte[] decompress(byte[] input) {
    try (ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
         GZIPInputStream zipStream = new GZIPInputStream(new ByteArrayInputStream(input))) {

      byte[] buffer = new byte[14];
      int len;
      while ((len = zipStream.read(buffer)) != -1) {
        outputStream.write(buffer, 0, len);
      }

      final byte[] bytes = outputStream.toByteArray();
      logger.info("Decompressed " + input.length + " bytes to " + bytes.length + " bytes");
      return bytes;
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

}
