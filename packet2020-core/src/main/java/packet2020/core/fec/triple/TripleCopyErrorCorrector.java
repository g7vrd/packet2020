package packet2020.core.fec.triple;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import packet2020.core.fec.ErrorCorrector;

import java.io.ByteArrayOutputStream;

/**
 * A very basic and inefficient form of FEC for testing.<br/>
 * Should look into Reed Solomon, or something a little more clever than this.
 */
public class TripleCopyErrorCorrector implements ErrorCorrector {

  private final Logger logger = LoggerFactory.getLogger(getClass());

  @Override
  public byte[] addFEC(byte[] input) {
    byte[] output = new byte[input.length * 3];

    System.arraycopy(input, 0, output, 0, input.length);
    System.arraycopy(input, 0, output, input.length, input.length);
    System.arraycopy(input, 0, output, input.length * 2, input.length);

    logger.info("Fec increased " + input.length + " bytes to " + output.length + " bytes");
    return output;
  }

  @Override
  public byte[] getCorrected(byte[] input) {
    if (input.length % 3 != 0) {
      throw new IllegalArgumentException("Not supported yet");
    }

    final int oneThird = input.length / 3;

    final ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
    for (int i = 0; i < oneThird; i = i + 1) {
      int aIndex = i;
      int bIndex = i + oneThird;
      int cIndex = i + (oneThird * 2);
      final byte a = input[aIndex];
      final byte b = input[bIndex];
      final byte c = input[cIndex];
      if (a == b || a == c) {
        outputStream.write(a);
      } else if (b == c) {
        outputStream.write(b);
      } else {
        throw new IllegalArgumentException("Too many errors to fix: - " + aIndex + "=" + a + " " + bIndex + "=" + b + " " + cIndex + "=" + c);
      }
    }

    final byte[] output = outputStream.toByteArray();
    logger.info("Fec reduced " + input.length + " bytes to " + output.length + " bytes");
    return output;
  }
}
