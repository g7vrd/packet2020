package packet2020.core.fec.hamming;

import org.springframework.stereotype.Component;
import packet2020.core.fec.ErrorCorrector;

import java.util.BitSet;

@Component
public class Hamming74ErrorCorrector implements ErrorCorrector {

  private final Hamming74Encoder hamming74Encoder = new Hamming74Encoder();
  private final Hamming74Decoder hamming74Decoder = new Hamming74Decoder();

  @Override
  public byte[] addFEC(byte[] input) {
    final BitSet inputBitSet = BitSet.valueOf(input);
    final BitSet outputBitSet = new BitSet((input.length / 4) * 7);

    int outputIndex = 0;
    for (int i = 0; i < input.length * 8; i = i + 4) {
      final boolean b4 = inputBitSet.get(i + 3);
      final boolean b3 = inputBitSet.get(i + 2);
      final boolean b2 = inputBitSet.get(i + 1);
      final boolean b1 = inputBitSet.get(i);

      boolean[] encoded = hamming74Encoder.encode(b4, b3, b2, b1);

      outputBitSet.set(outputIndex++, encoded[6]);
      outputBitSet.set(outputIndex++, encoded[5]);
      outputBitSet.set(outputIndex++, encoded[4]);
      outputBitSet.set(outputIndex++, encoded[3]);
      outputBitSet.set(outputIndex++, encoded[2]);
      outputBitSet.set(outputIndex++, encoded[1]);
      outputBitSet.set(outputIndex++, encoded[0]);
    }

    return outputBitSet.toByteArray();
  }

  @Override
  public byte[] getCorrected(byte[] input) {
    final BitSet inputBitSet = BitSet.valueOf(input);
    final BitSet outputBitSet = new BitSet((input.length / 7) * 4);

    int outputIndex = 0;
    for (int i = 0; i < inputBitSet.length(); i = i + 7) {
      final boolean b7 = inputBitSet.get(i + 6);
      final boolean b6 = inputBitSet.get(i + 5);
      final boolean b5 = inputBitSet.get(i + 4);
      final boolean b4 = inputBitSet.get(i + 3);
      final boolean b3 = inputBitSet.get(i + 2);
      final boolean b2 = inputBitSet.get(i + 1);
      final boolean b1 = inputBitSet.get(i);

      boolean[] decoded = hamming74Decoder.decode(b7, b6, b5, b4, b3, b2, b1);

      outputBitSet.set(outputIndex++, decoded[3]);
      outputBitSet.set(outputIndex++, decoded[2]);
      outputBitSet.set(outputIndex++, decoded[1]);
      outputBitSet.set(outputIndex++, decoded[0]);
    }


    return outputBitSet.toByteArray();
  }

  private String getString(boolean... bits) {
    StringBuilder ret = new StringBuilder();
    for (boolean b : bits) {
      if (b) {
        ret.append("1");
      } else {
        ret.append("0");
      }
    }

    return ret.toString();
  }

}
