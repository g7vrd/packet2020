package packet2020.core.fec.hamming;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;

class Hamming74Decoder {

  private final Logger logger = LoggerFactory.getLogger(getClass());

  private final static boolean[][] PARITY_CHECK = new boolean[][]{
    new boolean[]{true, false, true, false, true, false, true},
    new boolean[]{false, true, true, false, false, true, true},
    new boolean[]{false, false, false, true, true, true, true}
  };


  private final static boolean[][] DECODER = new boolean[][]{
    new boolean[]{false, false, true, false, false, false, false},
    new boolean[]{false, false, false, false, true, false, false},
    new boolean[]{false, false, false, false, false, true, false},
    new boolean[]{false, false, false, false, false, false, true},
  };


  boolean[] decode(boolean d4,
                   boolean d3,
                   boolean d2,
                   boolean p3,
                   boolean d1,
                   boolean p2,
                   boolean p1) {

    boolean[] parity = getParity(d4, d3, d2, p3, d1, p2, p1);

    boolean[] result = new boolean[7];
    int i = 0;
    for (boolean[] row : DECODER) {
      int res = 0;
      if (row[0] && d4) res++;
      if (row[1] && d3) res++;
      if (row[2] && d2) res++;
      if (row[3] && p3) res++;
      if (row[4] && d1) res++;
      if (row[5] && p2) res++;
      if (row[6] && p1) res++;

      result[i++] = res % 2 == 1;
    }


    // I don't understand this bit.
    if (Arrays.equals(parity, new boolean[]{true, true, false})) {
      logger.info("Corrected bit 0");
      result[0] = !result[0];
    }
    if (Arrays.equals(parity, new boolean[]{true, false, true})) {
      logger.info("Corrected bit 1");
      result[1] = !result[1];
    }
    if (Arrays.equals(parity, new boolean[]{false, true, true})) {
      logger.info("Corrected bit 2");
      result[2] = !result[2];
    }
    if (Arrays.equals(parity, new boolean[]{true, true, true})) {
      logger.info("Corrected bit 3");
      result[3] = !result[3];
    }

    return result;
  }

  private boolean[] getParity(boolean d4,
                              boolean d3,
                              boolean d2,
                              boolean p3,
                              boolean d1,
                              boolean p2,
                              boolean p1) {
    final boolean[] parity = new boolean[3];

    int i = 0;
    for (boolean[] row : PARITY_CHECK) {
      int res = 0;
      if (row[0] && d4) res++;
      if (row[1] && d3) res++;
      if (row[2] && d2) res++;
      if (row[3] && p3) res++;
      if (row[4] && d1) res++;
      if (row[5] && p2) res++;
      if (row[6] && p1) res++;

      parity[i++] = res % 2 == 1;
    }

    return parity;
  }

  private boolean c(boolean b1,
                    boolean b2,
                    boolean b3) {
    int ret = 0;
    if (b1) ret++;
    if (b2) ret++;
    if (b3) ret++;

    return ret % 2 == 0;
  }

  private String getString(boolean... bits) {
    StringBuilder ret = new StringBuilder();
    for (boolean b : bits) {
      if (b) {
        ret.append("1");
      } else {
        ret.append("0");
      }
    }

    return ret.toString();
  }
}
