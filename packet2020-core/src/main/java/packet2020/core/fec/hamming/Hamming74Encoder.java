package packet2020.core.fec.hamming;

class Hamming74Encoder {


  private final static boolean[][] GENERATOR = new boolean[][]{
    new boolean[]{true, true, false, true},
    new boolean[]{true, false, true, true},
    new boolean[]{true, false, false, false},
    new boolean[]{false, true, true, true},
    new boolean[]{false, true, false, false},
    new boolean[]{false, false, true, false},
    new boolean[]{false, false, false, true},
  };

  /**
   * Takes 4 bits, calculated 3 parity bits, returns 7 bits
   */
  boolean[] encode(boolean b4,
                   boolean b3,
                   boolean b2,
                   boolean b1) {

    final boolean[] result = new boolean[7];

    int i = 0;
    for (boolean[] row : GENERATOR) {
      int res = 0;
      if (row[0] && b4) res++;
      if (row[1] && b3) res++;
      if (row[2] && b2) res++;
      if (row[3] && b1) res++;

      result[i++] = res % 2 == 1;
    }

    return result;
  }


  private String getString(boolean... bits) {
    StringBuilder ret = new StringBuilder();
    for (boolean b : bits) {
      if (b) {
        ret.append("1");
      } else {
        ret.append("0");
      }
    }

    return ret.toString();
  }


}
