package packet2020.core.fec;

public interface ErrorCorrector {

  byte[] addFEC(byte[] input);

  byte[] getCorrected(byte[] input);
}
