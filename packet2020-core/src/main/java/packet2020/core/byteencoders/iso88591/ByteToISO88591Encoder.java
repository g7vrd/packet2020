package packet2020.core.byteencoders.iso88591;

import packet2020.core.byteencoders.ByteEncoder;

import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;

public class ByteToISO88591Encoder implements ByteEncoder {

  private final Charset charset = Charset.forName("ISO8859-1");


  @Override
  public char[] encode(byte[] input) {
    final CharBuffer charBuffer = charset.decode(ByteBuffer.wrap(input));

    return charBuffer.array();
  }

  @Override
  public byte[] decode(char[] input) {
    final ByteBuffer byteBuffer = charset.encode(CharBuffer.wrap(input));

    return byteBuffer.array();
  }
}
