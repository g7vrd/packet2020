package packet2020.core.byteencoders.olivia;

import org.springframework.stereotype.Component;
import packet2020.core.byteencoders.ByteEncoder;
import packet2020.core.byteencoders.Converter;

import java.io.ByteArrayOutputStream;

@Component
public class ByteToOliviaEncoder implements ByteEncoder {

  private final Converter converter = new OliviaConverter();

  @Override
  public char[] encode(byte[] input) {
    final StringBuilder stringBuilder = new StringBuilder();
    for (byte b : input) {
      final int lower = b & 0xF;
      stringBuilder.append(converter.intToChar(lower));
      final int higher = (b >> 4) & 0xF;
      stringBuilder.append(converter.intToChar(higher));
    }

    return stringBuilder.toString().toCharArray();
  }

  @Override
  public byte[] decode(char[] input) {
    final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

    for (int i = 0; i < input.length; i = i + 2) {
      int lower = converter.charToInt(input[i]);
      int higher = converter.charToInt(input[i + 1]);

      byte b = (byte) (lower + (higher << 4));
      byteArrayOutputStream.write(b);
    }

    return byteArrayOutputStream.toByteArray();
  }


  private class OliviaConverter implements Converter {

    // These chars have been picked because they are the characters with the fewest symbols.

    @Override
    public Integer charToInt(Character c) {
      switch (c) {
        case (char) 32:
          return 0;
        case (char) 101:
          return 1;
        case (char) 116:
          return 2;
        case (char) 97:
          return 3;
        case (char) 105:
          return 4;
        case (char) 110:
          return 5;
        case (char) 111:
          return 6;
        case (char) 99:
          return 7;
        case (char) 100:
          return 8;
        case (char) 104:
          return 9;
        case (char) 108:
          return 10;
        case (char) 114:
          return 11;
        case (char) 115:
          return 12;
        case (char) 117:
          return 13;
        case (char) 98:
          return 14;
        case (char) 102:
          return 15;
        default:
          return 0;
//          throw new IllegalArgumentException("" + c);
      }
    }


    @Override
    public Character intToChar(Integer i) {
      switch (i) {
        case 0:
          return (char) 32;
        case 1:
          return (char) 101;
        case 2:
          return (char) 116;
        case 3:
          return (char) 97;
        case 4:
          return (char) 105;
        case 5:
          return (char) 110;
        case 6:
          return (char) 111;
        case 7:
          return (char) 99;
        case 8:
          return (char) 100;
        case 9:
          return (char) 104;
        case 10:
          return (char) 108;
        case 11:
          return (char) 114;
        case 12:
          return (char) 115;
        case 13:
          return (char) 117;
        case 14:
          return (char) 98;
        case 15:
          return (char) 102;

        default:
          return (char) 32;
//          throw new IllegalArgumentException("" + i);
      }
    }
  }
}
