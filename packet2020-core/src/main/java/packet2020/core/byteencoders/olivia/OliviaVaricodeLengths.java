package packet2020.core.byteencoders.olivia;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class OliviaVaricodeLengths {


  private static final List<Character> THREE = Collections.singletonList((char) 32);
  private static final List<Character> FOUR = Arrays.asList((char) 101, (char) 116);
  private static final List<Character> FIVE = Arrays.asList((char) 97, (char) 105, (char) 110, (char) 111);
  private static final List<Character> SIX = Arrays.asList(
    (char) 99, (char) 100, (char) 104, (char) 108, (char) 114, (char) 115, (char) 117);
  private static final List<Character> SEVEN = Arrays.asList(
    (char) 98, (char) 102, (char) 103, (char) 107, (char) 109, (char) 112, (char) 113, (char) 118, (char) 119,
    (char) 120, (char) 121, (char) 122);

  private static final List<Character> EIGHT = Arrays.asList(
    (char) 8, (char) 13, (char) 44, (char) 48, (char) 49, (char) 65, (char) 67, (char) 68, (char) 69, (char) 70,
    (char) 73, (char) 76, (char) 77, (char) 78, (char) 79, (char) 80, (char) 82, (char) 83, (char) 84, (char) 106
  );
  private static final List<Character> NINE = Arrays.asList(
    (char) 33, (char) 34, (char) 39, (char) 40, (char) 41, (char) 43, (char) 45, (char) 46, (char) 47, (char) 50,
    (char) 51, (char) 52, (char) 53, (char) 54, (char) 55, (char) 56, (char) 57, (char) 58, (char) 59, (char) 61,
    (char) 63, (char) 66, (char) 71, (char) 72, (char) 74, (char) 75, (char) 81, (char) 85, (char) 86, (char) 87,
    (char) 88, (char) 89, (char) 90
  );

  private static final List<Character> TEN = Arrays.asList(
    (char) 35, (char) 36, (char) 37, (char) 38, (char) 42, (char) 60, (char) 62, (char) 64, (char) 91, (char) 92,
    (char) 93, (char) 94, (char) 95, (char) 96, (char) 123, (char) 124, (char) 125, (char) 126, (char) 160, (char) 161,
    (char) 162, (char) 163, (char) 164, (char) 165, (char) 166, (char) 167, (char) 168, (char) 169, (char) 170,
    (char) 171, (char) 172, (char) 173, (char) 174, (char) 175, (char) 176, (char) 177, (char) 178, (char) 179,
    (char) 180, (char) 181, (char) 182, (char) 183, (char) 184, (char) 185, (char) 186, (char) 187, (char) 188,
    (char) 189, (char) 190, (char) 191, (char) 192, (char) 193, (char) 194, (char) 195
  );

  private static final List<Character> TWELVE = Arrays.asList(
    (char) 30, (char) 31, (char) 127, (char) 128, (char) 129, (char) 130, (char) 131, (char) 132, (char) 133,
    (char) 134, (char) 135, (char) 136, (char) 137, (char) 138, (char) 139, (char) 140, (char) 141, (char) 142,
    (char) 143, (char) 144, (char) 145, (char) 146, (char) 147, (char) 148, (char) 149, (char) 150, (char) 151,
    (char) 152, (char) 153, (char) 154, (char) 155, (char) 156, (char) 157, (char) 158, (char) 159
  );

  public static int getLength(char c) {
    if (THREE.contains(c)) return 3;
    if (FOUR.contains(c)) return 4;
    if (FIVE.contains(c)) return 5;
    if (SIX.contains(c)) return 6;
    if (SEVEN.contains(c)) return 7;
    if (EIGHT.contains(c)) return 8;
    if (NINE.contains(c)) return 9;
    if (TEN.contains(c)) return 10;
    if (TWELVE.contains(c)) return 12;

    return 11;
  }

}
