package packet2020.core.byteencoders;

/**
 * Encodes a byte array into an array of chars that is supported by the intended digimode
 */
public interface ByteEncoder {

  char[] encode(byte[] input);

  byte[] decode(char[] input);
}
