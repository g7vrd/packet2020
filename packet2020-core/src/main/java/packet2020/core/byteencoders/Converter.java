package packet2020.core.byteencoders;

public interface Converter {

  Integer charToInt(Character c);

  Character intToChar(Integer i);
}
