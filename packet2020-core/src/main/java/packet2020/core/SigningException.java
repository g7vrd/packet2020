package packet2020.core;

public class SigningException extends RuntimeException {

  public SigningException(String s) {
    super(s);
  }
}
