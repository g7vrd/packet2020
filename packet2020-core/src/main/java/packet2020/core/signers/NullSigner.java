package packet2020.core.signers;

public class NullSigner implements Signer {

  @Override
  public byte[] addSignature(byte[] input) {
    return input;
  }

  @Override
  public byte[] verify(byte[] input) {
    return input;
  }
}
