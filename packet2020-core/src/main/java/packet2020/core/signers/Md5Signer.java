package packet2020.core.signers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import packet2020.core.SigningException;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

@Component
public class Md5Signer implements Signer {

  private final int signatureLength;

  @Autowired
  public Md5Signer(@Value("${signer.md5.bytes}") int signatureLength) {
    this.signatureLength = signatureLength;
    if (signatureLength > 32) {
      throw new RuntimeException("signatureLength should be <= 32");
    }
  }

  @Override
  public byte[] addSignature(byte[] input) {

    final byte[] md5sum = getMd5sum(input);

    final byte[] result = new byte[input.length + this.signatureLength];

    System.arraycopy(input, 0, result, 0, input.length);
    System.arraycopy(md5sum, 0, result, input.length, signatureLength);

    return result;
  }

  @Override
  public byte[] verify(byte[] input) {
    if (input.length <= signatureLength)
      throw new SigningException("Input is not long enough (" + input.length + ") to contain a signature (" + signatureLength + ") and data");

    byte[] data = new byte[input.length - signatureLength];
    byte[] signature = new byte[signatureLength];

    System.arraycopy(input, 0, data, 0, input.length - signatureLength);
    System.arraycopy(input, input.length - signatureLength, signature, 0, signatureLength);

    byte[] fullDigest = getMd5sum(data);
    byte[] digest = new byte[signatureLength];
    System.arraycopy(fullDigest, 0, digest, 0, signatureLength);

    if (!Arrays.equals(digest, signature)) {
      throw new SigningException("Signatures do not match: " + Arrays.toString(signature) + " != " + Arrays.toString(digest));
    }

    return data;
  }

  private byte[] getMd5sum(byte[] input) {
    try {
      MessageDigest md = MessageDigest.getInstance("MD5");
      md.update(input);
      return md.digest();
    } catch (NoSuchAlgorithmException e) {
      throw new RuntimeException(e);
    }
  }
}
