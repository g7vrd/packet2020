package packet2020.core.signers;

public interface Signer {

  byte[] addSignature(byte[] input);

  byte[] verify(byte[] input);
}
