package packet2020.core.receiver;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import packet2020.client.api.Client;
import packet2020.core.byteencoders.ByteEncoder;
import packet2020.core.compressors.Compressor;
import packet2020.core.fec.ErrorCorrector;
import packet2020.core.signers.Signer;

import java.util.Base64;
import java.util.Optional;

@Service
public class Receiver extends Thread {

  private final Logger logger = LoggerFactory.getLogger(getClass());

  private final Client client;
  private final ByteEncoder byteEncoder;
  private final ErrorCorrector errorCorrector;
  private final Signer signer;
  private final Compressor compressor;

  private volatile boolean running = true;

  private final StringBuilder rxData = new StringBuilder();

  @Autowired
  public Receiver(Client client,
                  ByteEncoder byteEncoder,
                  ErrorCorrector errorCorrector,
                  Signer signer,
                  Compressor compressor) {
    this.client = client;
    this.byteEncoder = byteEncoder;
    this.errorCorrector = errorCorrector;
    this.signer = signer;
    this.compressor = compressor;
  }

  @Override
  public void run() {
    while (running) {

      if (client.isReceiving()) {
        final Optional<String> latestRxData = client.getLatestRxData();
        if (latestRxData.isPresent()) {
          final byte[] bytes = Base64.getDecoder().decode(latestRxData.get());

          rxData.append(new String(bytes));
          attemptDecode(rxData.toString());
        }
      }

      zzzz();
    }
  }

  private void attemptDecode(String allChars) {
    for (int start = 0; start < allChars.length(); start++) {
      for (int end = allChars.length(); end > start; end--) {
        try {
          final String attempt = allChars.substring(start, end);
          final byte[] message = getMessage(attempt);

          if (message.length > 3) {
            System.out.println(start + "-" + end + ": >>" + attempt + "<<");
            System.out.println(">>" + new String(message) + "<<");
          }
        } catch (Exception e) {
        }
      }
    }
  }

  private byte[] getMessage(String substring) {
    final byte[] decoded = byteEncoder.decode(substring.toCharArray());
    final byte[] corrected = errorCorrector.getCorrected(decoded);
    final byte[] verified = signer.verify(corrected);
    return compressor.decompress(verified);
  }

  private void zzzz() {
    try {
      Thread.sleep(1000);
    } catch (InterruptedException e) {
      throw new RuntimeException(e);
    }
  }
}
