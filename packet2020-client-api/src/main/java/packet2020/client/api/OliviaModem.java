package packet2020.client.api;

public enum OliviaModem implements Modem {

  OLIVIA_4_250("Olivia-4-250"),
  OLIVIA_8_250("Olivia-8-250"),
  OLIVIA_4_500("Olivia-4-500"),
  OLIVIA_8_500("Olivia-8-500"),
  OLIVIA_16_500("Olivia-16-500"),
  OLIVIA_8_1K("Olivia-8-1K"),
  OLIVIA_16_1K("Olivia-16-1K"),
  OLIVIA_32_1K("Olivia-32-1K"),
  OLIVIA_64_2K("Olivia-64-2K");

  private final String fldigi;

  OliviaModem(String fldigi) {
    this.fldigi = fldigi;
  }

  @Override
  public String getFldigi() {
    return fldigi;
  }
}
