package packet2020.client.api;

import java.util.Optional;

/**
 * Interface defining operations that can be performed on a radio.
 */
public interface Client {

  boolean isReceiving();

  void setFrequency(int hz);

  void setModemCarrierFrequency(int hz);

  void setMode(Mode mode);

  void setModem(Modem modem);

  void clearRxText();

  void clearTxText();

  void addTxText(String text);

  void setRsId(boolean enabled);

  void setTxId(boolean enabled);

  void transmit();

  Optional<String> getLatestRxData();
}
