package packet2020.client.xmlrpc;

import com.github.restdriver.clientdriver.ClientDriverRule;
import org.junit.Rule;
import org.junit.Test;
import org.springframework.web.client.RestTemplate;
import packet2020.client.xmlrpc.commands.SetFrequencyCommand;
import packet2020.client.xmlrpc.commands.CommandSender;

import static org.assertj.core.api.Assertions.assertThat;

public class DoubleCommandTest extends CommandTest {

  @Rule
  public ClientDriverRule xmlRpcServer = new ClientDriverRule();


  @Test
  public void SetFrequencyCommmand_produces_correctly_formatted_request() {
    CommandSender commandSender = new CommandSender(new RestTemplate(), xmlRpcServer.getBaseUrl() + "/RPC2");
    final String methodName = "rig.set_frequency";
    final String freq = "14105540.00000";

    xmlRpcServer.addExpectation(
      doubleRequestExpectation(methodName, freq),
      doubleResponseExpectation("9999"));

    final String response = commandSender.send(new SetFrequencyCommand(freq));

    assertThat(response).isEqualTo("<?xml version=\"1.0\"?><methodResponse><params><param><value><double>9999</double></value></param></params></methodResponse>");
  }

}
