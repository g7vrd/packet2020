package packet2020.client.xmlrpc;

import com.github.restdriver.clientdriver.ClientDriverRule;
import org.junit.Rule;
import org.junit.Test;
import org.springframework.web.client.RestTemplate;
import packet2020.client.xmlrpc.commands.SetModemCarrierCommand;
import packet2020.client.xmlrpc.commands.CommandSender;

import static org.assertj.core.api.Assertions.assertThat;

public class IntCommandTest extends CommandTest {

  @Rule
  public ClientDriverRule xmlRpcServer = new ClientDriverRule();


  @Test
  public void SetModemCarrierRequest_produces_correctly_formatted_request() {
    CommandSender commandSender = new CommandSender(new RestTemplate(), xmlRpcServer.getBaseUrl() + "/RPC2");
    final String methodName = "modem.set_carrier";
    final int carrier = 1000;

    xmlRpcServer.addExpectation(
      intRequestExpectation(methodName, carrier),
      i4ResponseExpectation(9999));

    final String response = commandSender.send(new SetModemCarrierCommand(carrier));

    assertThat(response).isEqualTo("<?xml version=\"1.0\"?><methodResponse><params><param><value><i4>9999</i4></value></param></params></methodResponse>");
  }

}
