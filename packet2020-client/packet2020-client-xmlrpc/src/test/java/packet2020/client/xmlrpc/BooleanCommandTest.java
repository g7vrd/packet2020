package packet2020.client.xmlrpc;

import com.github.restdriver.clientdriver.ClientDriverRule;
import org.junit.Rule;
import org.junit.Test;
import org.springframework.web.client.RestTemplate;
import packet2020.client.xmlrpc.commands.SetRsIdCommand;
import packet2020.client.xmlrpc.commands.SetTxIdCommand;
import packet2020.client.xmlrpc.commands.CommandSender;

import static org.assertj.core.api.Assertions.assertThat;

public class BooleanCommandTest extends CommandTest {

  @Rule
  public ClientDriverRule xmlRpcServer = new ClientDriverRule();


  @Test
  public void SetRsIdRequest_produces_correctly_formatted_request() {
    CommandSender commandSender = new CommandSender(new RestTemplate(), xmlRpcServer.getBaseUrl() + "/RPC2");
    final String methodName = "main.set_rsid";
    final boolean enabled = true;

    xmlRpcServer.addExpectation(
      booleanRequestExpectation(methodName, enabled),
      booleanResponseExpectation(true));

    final String response = commandSender.send(new SetRsIdCommand(enabled));

    assertThat(response).isEqualTo("<?xml version=\"1.0\"?><methodResponse><params><param><value><boolean>1</boolean></value></param></params></methodResponse>");
  }

  @Test
  public void SetTxIdRequest_produces_correctly_formatted_request() {
    CommandSender commandSender = new CommandSender(new RestTemplate(), xmlRpcServer.getBaseUrl() + "/RPC2");
    final String methodName = "main.set_txid";
    final boolean enabled = true;

    xmlRpcServer.addExpectation(
      booleanRequestExpectation(methodName, enabled),
      booleanResponseExpectation(false));

    final String response = commandSender.send(new SetTxIdCommand(enabled));

    assertThat(response).isEqualTo("<?xml version=\"1.0\"?><methodResponse><params><param><value><boolean>0</boolean></value></param></params></methodResponse>");
  }
}
