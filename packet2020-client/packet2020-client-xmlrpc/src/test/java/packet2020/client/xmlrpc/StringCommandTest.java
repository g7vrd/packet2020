package packet2020.client.xmlrpc;

import com.github.restdriver.clientdriver.ClientDriverRule;
import org.junit.Rule;
import org.junit.Test;
import org.springframework.web.client.RestTemplate;
import packet2020.client.xmlrpc.commands.AddTextCommand;
import packet2020.client.xmlrpc.commands.SetModeCommand;
import packet2020.client.xmlrpc.commands.SetModemByNameCommand;
import packet2020.client.xmlrpc.commands.CommandSender;

import static org.assertj.core.api.Assertions.assertThat;

public class StringCommandTest extends CommandTest {

  @Rule
  public ClientDriverRule xmlRpcServer = new ClientDriverRule();

  @Test
  public void SetModeRequest_produces_correctly_formatted_request() {
    CommandSender commandSender = new CommandSender(new RestTemplate(), xmlRpcServer.getBaseUrl() + "/RPC2");
    final String methodName = "rig.set_mode";
    final String mode = "PKTUSB";

    xmlRpcServer.addExpectation(
      stringRequestExpectation(methodName, mode),
      emptyResponseExpectation());

    final String response = commandSender.send(new SetModeCommand(mode));

    assertThat(response).isEqualTo("<?xml version=\"1.0\"?><methodResponse><params><param><value></value></param></params></methodResponse>");
  }

  @Test
  public void SetModemByNameRequest_produces_correctly_formatted_request() {
    CommandSender commandSender = new CommandSender(new RestTemplate(), xmlRpcServer.getBaseUrl() + "/RPC2");
    final String methodName = "modem.set_by_name";
    final String modemName = "Olivia-32-1K";

    xmlRpcServer.addExpectation(
      stringRequestExpectation(methodName, modemName),
      emptyResponseExpectation());


    final String response = commandSender.send(new SetModemByNameCommand(modemName));

    assertThat(response).isEqualTo("<?xml version=\"1.0\"?><methodResponse><params><param><value></value></param></params></methodResponse>");
  }

  @Test
  public void AddTextRequest_produces_correctly_formatted_request() {
    CommandSender commandSender = new CommandSender(new RestTemplate(), xmlRpcServer.getBaseUrl() + "/RPC2");
//    CommandSender commandSender = new CommandSender(new RestTemplate(), "http://127.0.0.1:7362/RPC2");
    final String methodName = "text.add_tx";
    final String text = "Some text";

    xmlRpcServer.addExpectation(
      stringRequestExpectation(methodName, text),
      emptyResponseExpectation());


    final String response = commandSender.send(new AddTextCommand(text));

    assertThat(response).isEqualTo("<?xml version=\"1.0\"?><methodResponse><params><param><value></value></param></params></methodResponse>");
  }

}
