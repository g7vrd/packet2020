package packet2020.client.xmlrpc;

import com.github.restdriver.clientdriver.ClientDriverRule;
import org.junit.Rule;
import org.junit.Test;
import org.springframework.web.client.RestTemplate;
import packet2020.client.xmlrpc.commands.*;
import packet2020.client.xmlrpc.commands.CommandSender;

import static org.assertj.core.api.Assertions.assertThat;

public class NoParamCommandTest extends CommandTest {

  @Rule
  public ClientDriverRule xmlRpcServer = new ClientDriverRule();


  @Test
  public void TxRequest_produces_correctly_formatted_request() {
    CommandSender commandSender = new CommandSender(new RestTemplate(), xmlRpcServer.getBaseUrl() + "/RPC2");
    final String methodName = "main.tx";

    xmlRpcServer.addExpectation(
      emptyRequestExpectation(methodName),
      emptyResponseExpectation());

    final String response = commandSender.send(new TxCommand());

    assertThat(response).isEqualTo("<?xml version=\"1.0\"?><methodResponse><params><param><value></value></param></params></methodResponse>");
  }

  @Test
  public void RxRequest_produces_correctly_formatted_request() {
    CommandSender commandSender = new CommandSender(new RestTemplate(), xmlRpcServer.getBaseUrl() + "/RPC2");
    final String methodName = "main.rx";

    xmlRpcServer.addExpectation(
      emptyRequestExpectation(methodName),
      emptyResponseExpectation());

    final String response = commandSender.send(new RxCommand());

    assertThat(response).isEqualTo("<?xml version=\"1.0\"?><methodResponse><params><param><value></value></param></params></methodResponse>");
  }


  @Test
  public void GetFreqRequest_produces_correctly_formatted_request() {
    CommandSender commandSender = new CommandSender(new RestTemplate(), xmlRpcServer.getBaseUrl() + "/RPC2");
    final String methodName = "rig.get_frequency";

    xmlRpcServer.addExpectation(
      emptyRequestExpectation(methodName),
      doubleResponseExpectation("145252.000000"));

    final String response = commandSender.send(new GetFrequencyCommand());

    assertThat(response).isEqualTo("<?xml version=\"1.0\"?><methodResponse><params><param><value><double>145252.000000</double></value></param></params></methodResponse>");
  }

  @Test
  public void GetRsIdRequest_produces_correctly_formatted_request() {
    CommandSender commandSender = new CommandSender(new RestTemplate(), xmlRpcServer.getBaseUrl() + "/RPC2");
    final String methodName = "main.get_rsid";

    xmlRpcServer.addExpectation(
      emptyRequestExpectation(methodName),
      booleanResponseExpectation(true));

    final String response = commandSender.send(new GetRsIdCommand());

    assertThat(response).isEqualTo("<?xml version=\"1.0\"?><methodResponse><params><param><value><boolean>1</boolean></value></param></params></methodResponse>");
  }

  @Test
  public void GetRxDataRequest_produces_correctly_formatted_request() {
    CommandSender commandSender = new CommandSender(new RestTemplate(), xmlRpcServer.getBaseUrl() + "/RPC2");
    final String methodName = "rx.get_data";

    xmlRpcServer.addExpectation(
      emptyRequestExpectation(methodName),
      base64ResponseExpectation("text"));

    final String response = commandSender.send(new GetRxDataCommand());

    assertThat(response).isEqualTo("<?xml version=\"1.0\"?><methodResponse><params><param><value><base64>dGV4dA==</base64></value></param></params></methodResponse>");
  }
}
