package packet2020.client.xmlrpc;

import com.github.restdriver.clientdriver.ClientDriverRequest;
import com.github.restdriver.clientdriver.ClientDriverResponse;

import java.util.Base64;

abstract class CommandTest {

  ClientDriverRequest emptyRequestExpectation(String methodName) {
    return new ClientDriverRequest("/RPC2")
      .withMethod(ClientDriverRequest.Method.POST)
      .withBody("<?xml version=\"1.0\" encoding=\"us-ascii\"?><methodCall><methodName>" + methodName + "</methodName><params></params></methodCall>", "text/xml");
  }

  ClientDriverRequest stringRequestExpectation(String methodName,
                                               String stringValue) {
    return new ClientDriverRequest("/RPC2")
      .withMethod(ClientDriverRequest.Method.POST)
      .withBody("<?xml version=\"1.0\" encoding=\"us-ascii\"?><methodCall><methodName>" + methodName + "</methodName><params><param><value><string>" + stringValue + "</string></value></param></params></methodCall>", "text/xml");
  }

  ClientDriverRequest intRequestExpectation(String methodName,
                                            int intValue) {
    return new ClientDriverRequest("/RPC2")
      .withMethod(ClientDriverRequest.Method.POST)
      .withBody("<?xml version=\"1.0\" encoding=\"us-ascii\"?><methodCall><methodName>" + methodName + "</methodName><params><param><value><int>" + intValue + "</int></value></param></params></methodCall>", "text/xml");
  }

  ClientDriverRequest doubleRequestExpectation(String methodName,
                                               String doubleValue) {
    return new ClientDriverRequest("/RPC2")
      .withMethod(ClientDriverRequest.Method.POST)
      .withBody("<?xml version=\"1.0\" encoding=\"us-ascii\"?><methodCall><methodName>" + methodName + "</methodName><params><param><value><double>" + doubleValue + "</double></value></param></params></methodCall>", "text/xml");
  }

  ClientDriverRequest booleanRequestExpectation(String methodName,
                                                boolean enabled) {
    return new ClientDriverRequest("/RPC2")
      .withMethod(ClientDriverRequest.Method.POST)
      .withBody("<methodCall><methodName>" + methodName + "</methodName><params><param><value><boolean>" + (enabled ? "1" : "0") + "</boolean></value></param></params></methodCall>", "text/xml");
  }


  ClientDriverResponse emptyResponseExpectation() {
    return new ClientDriverResponse("<?xml version=\"1.0\"?><methodResponse><params><param><value></value></param></params></methodResponse>", "text/xml");
  }


  ClientDriverResponse i4ResponseExpectation(int value) {
    return new ClientDriverResponse("<?xml version=\"1.0\"?><methodResponse><params><param><value><i4>" + value + "</i4></value></param></params></methodResponse>", "text/xml");
  }

  ClientDriverResponse base64ResponseExpectation(String value) {
    final String encoded = Base64.getEncoder().encodeToString(value.getBytes());
    return new ClientDriverResponse("<?xml version=\"1.0\"?><methodResponse><params><param><value><base64>" + encoded + "</base64></value></param></params></methodResponse>", "text/xml");
  }

  ClientDriverResponse doubleResponseExpectation(String value) {
    return new ClientDriverResponse("<?xml version=\"1.0\"?><methodResponse><params><param><value><double>" + value + "</double></value></param></params></methodResponse>", "text/xml");
  }

  ClientDriverResponse booleanResponseExpectation(boolean value) {
    return new ClientDriverResponse("<?xml version=\"1.0\"?><methodResponse><params><param><value><boolean>" + (value ? "1" : "0") + "</boolean></value></param></params></methodResponse>", "text/xml");
  }
}
