package packet2020.client.xmlrpc.commands;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

public interface Command {

  HttpEntity<String> getXml();

  String getMethodName();

  default HttpHeaders getHttpHeaders() {
    HttpHeaders headers = new HttpHeaders();
    headers.setContentType(MediaType.TEXT_XML);
    return headers;
  }
}
