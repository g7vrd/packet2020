package packet2020.client.xmlrpc.commands;

import org.springframework.http.HttpEntity;

public class AddTextCommand implements Command {

  private final String text;

  public AddTextCommand(String text) {
    this.text = text;
  }

  @Override
  public HttpEntity<String> getXml() {
    return new HttpEntity<>("<?xml version=\"1.0\" encoding=\"us-ascii\"?><methodCall><methodName>" + getMethodName() + "</methodName><params><param><value><string>" + text + "</string></value></param></params></methodCall>", getHttpHeaders());
  }

  @Override
  public String getMethodName() {
    return "text.add_tx";
  }
}
