package packet2020.client.xmlrpc.commands;

import org.springframework.http.HttpEntity;

public class SetModemByNameCommand implements Command {

  private final String modemName;

  public SetModemByNameCommand(String modemName) {
    this.modemName = modemName;
  }

  @Override
  public HttpEntity<String> getXml() {
    return new HttpEntity<>("<?xml version=\"1.0\" encoding=\"us-ascii\"?><methodCall><methodName>" + getMethodName() + "</methodName><params><param><value><string>" + modemName + "</string></value></param></params></methodCall>", getHttpHeaders());
  }

  @Override
  public String getMethodName() {
    return "modem.set_by_name";
  }
}
