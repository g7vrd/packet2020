package packet2020.client.xmlrpc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import packet2020.client.api.Client;
import packet2020.client.api.Mode;
import packet2020.client.api.Modem;
import packet2020.client.xmlrpc.commands.*;

import java.util.Optional;

@Service
public class XmlRpcClient implements Client {

  private final CommandSender commandSender;

  @Autowired
  public XmlRpcClient(CommandSender commandSender) {
    this.commandSender = commandSender;
  }

  @Override
  public boolean isReceiving() {
    final String response = commandSender.send(new GetTxRxStateCommand());
    return response.contains("<value>rx</value>");
  }

  @Override
  public void setFrequency(int hz) {
    commandSender.send(new SetFrequencyCommand(String.valueOf(hz)));
  }

  @Override
  public void setModemCarrierFrequency(int hz) {
    commandSender.send(new SetModemCarrierCommand(hz));
  }

  @Override
  public void setMode(Mode mode) {
    commandSender.send(new SetModeCommand(mode.name()));
  }

  @Override
  public void setModem(Modem modem) {
    commandSender.send(new SetModemByNameCommand(modem.getFldigi()));
  }

  @Override
  public void clearRxText() {
    commandSender.send(new ClearRxCommand());
  }

  @Override
  public void clearTxText() {
    commandSender.send(new ClearTxCommand());
  }

  @Override
  public void addTxText(String text) {
    commandSender.send(new AddTextCommand(text));
  }

  @Override
  public void setRsId(boolean enabled) {
    commandSender.send(new SetRsIdCommand(enabled));
  }

  @Override
  public void setTxId(boolean enabled) {
    commandSender.send(new SetTxIdCommand(enabled));
  }

  @Override
  public void transmit() {
    commandSender.send(new TxCommand());
  }

  @Override
  public Optional<String> getLatestRxData() {
    final String response = commandSender.send(new GetRxDataCommand());
    final int start = response.indexOf("<value><base64>") + 15;
    final int end = response.indexOf("</base64></value>");
    if (end == 72) return Optional.empty(); // I don't like this magic number...
    return Optional.of(response.substring(start, end));
  }
}
