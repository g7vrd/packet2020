package packet2020.client.xmlrpc.commands;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;
import org.springframework.web.client.RestTemplate;

@Component
public class CommandSender {

  private final RestTemplate restTemplate;
  private final String url;

  public CommandSender(RestTemplate restTemplate,
                       @Value("${url}") String url) {
    this.restTemplate = restTemplate;
    this.url = url;
  }

  public String send(Command request) {
    ResponseEntity<String> response = restTemplate.postForEntity(url, request.getXml(), String.class);

    Assert.isTrue(response.getStatusCodeValue() == 200, "HTTP Status was " + response.getStatusCodeValue());
    Assert.isTrue(response.getHeaders().get("Content-Type").get(0).equals("text/xml"));

    return response.getBody();
  }
}
