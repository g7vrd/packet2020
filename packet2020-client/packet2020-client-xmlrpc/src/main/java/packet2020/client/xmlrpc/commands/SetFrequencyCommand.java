package packet2020.client.xmlrpc.commands;

import org.springframework.http.HttpEntity;

public class SetFrequencyCommand implements Command {

  private final String frequency;

  public SetFrequencyCommand(String frequency) {
    this.frequency = frequency;
  }

  @Override
  public HttpEntity<String> getXml() {
    return new HttpEntity<>("<?xml version=\"1.0\" encoding=\"us-ascii\"?><methodCall><methodName>" + getMethodName() + "</methodName><params><param><value><double>" + frequency + "</double></value></param></params></methodCall>", getHttpHeaders());
  }

  @Override
  public String getMethodName() {
    return "rig.set_frequency";
  }
}
