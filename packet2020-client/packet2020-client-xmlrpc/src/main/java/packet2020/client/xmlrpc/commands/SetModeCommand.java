package packet2020.client.xmlrpc.commands;

import org.springframework.http.HttpEntity;

public class SetModeCommand implements Command {

  private final String mode;

  public SetModeCommand(String mode) {
    this.mode = mode;
  }

  @Override
  public HttpEntity<String> getXml() {
    return new HttpEntity<>("<?xml version=\"1.0\" encoding=\"us-ascii\"?><methodCall><methodName>" + getMethodName() + "</methodName><params><param><value><string>" + mode + "</string></value></param></params></methodCall>", getHttpHeaders());
  }

  @Override
  public String getMethodName() {
    return "rig.set_mode";
  }
}
