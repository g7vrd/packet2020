package packet2020.client.xmlrpc.commands;

import org.springframework.http.HttpEntity;

public class SetModemCarrierCommand implements Command {

  private final int carrier;

  public SetModemCarrierCommand(int carrier) {
    this.carrier = carrier;
  }

  @Override
  public HttpEntity<String> getXml() {
    return new HttpEntity<>("<?xml version=\"1.0\" encoding=\"us-ascii\"?><methodCall><methodName>" + getMethodName() + "</methodName><params><param><value><int>" + carrier + "</int></value></param></params></methodCall>", getHttpHeaders());
  }

  @Override
  public String getMethodName() {
    return "modem.set_carrier";
  }
}
