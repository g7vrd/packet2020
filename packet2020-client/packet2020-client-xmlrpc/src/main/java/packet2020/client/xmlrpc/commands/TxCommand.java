package packet2020.client.xmlrpc.commands;

import org.springframework.http.HttpEntity;

public class TxCommand implements Command {

  @Override
  public HttpEntity<String> getXml() {
    return new HttpEntity<>("<?xml version=\"1.0\" encoding=\"us-ascii\"?><methodCall><methodName>" + getMethodName() + "</methodName><params></params></methodCall>", getHttpHeaders());
  }

  @Override
  public String getMethodName() {
    return "main.tx";
  }
}
