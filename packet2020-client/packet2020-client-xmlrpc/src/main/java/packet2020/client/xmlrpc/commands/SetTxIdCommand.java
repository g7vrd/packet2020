package packet2020.client.xmlrpc.commands;

import org.springframework.http.HttpEntity;

public class SetTxIdCommand implements Command {

  private final boolean enabled;

  public SetTxIdCommand(boolean enabled) {
    this.enabled = enabled;
  }

  @Override
  public HttpEntity<String> getXml() {
    return new HttpEntity<>("<methodCall><methodName>" + getMethodName() + "</methodName><params><param><value><boolean>" + (enabled ? "1" : "0") + "</boolean></value></param></params></methodCall>", getHttpHeaders());
  }

  @Override
  public String getMethodName() {
    return "main.set_txid";
  }
}
